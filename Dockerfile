FROM ubuntu
RUN apt-get -y update
RUN apt-get install -y iproute2
RUN apt-get install -y net-tools
RUN apt-get install -y iputils-ping
RUN apt-get install -y default-jre
RUN apt-get install -y wget
WORKDIR /home
CMD ["/bin/bash"]
